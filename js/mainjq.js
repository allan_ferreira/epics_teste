$(document).ready(function(){
	var bankSelectet = 0;
	var partAtual = 1;
    $(".image-bank").click(function(){
    	$('#part_1').hide();
    	$('#part_2').show();
    	$('#backButton').show();
    	bankSelectet = $(this).attr('value');
    	$('#bankSelected').attr("src", "img/bank_"+bankSelectet+".png");
    });
    $('#submitForm').click(function(){
		$('#part_2').hide();
		$('#part_3').show();
		move();
		setTimeout(function(){
			$('#part_3').hide();
			$('#part_4').show();
		}, 2000);
    });
    $('#allowAccess').click(function(){
    	if (bankSelectet != 6) {
			$('#part_4').hide();
			$('#part_5').show();
    		$('#backButton').hide();
    		setTimeout(function(){
				$("#imgSuccess").fadeIn();
				setTimeout(function(){
					$("#messageSuccess").fadeIn();
				}, 300);
			}, 500);
    	}else{
			$('#part_4').hide();
			$('#part_6').show();
    		$('#backButton').hide();
    		setTimeout(function(){
				$("#imgFailure").fadeIn();
				setTimeout(function(){
					$("#messageFailure").fadeIn();
				}, 300);
			}, 500);
    	}
    });
    $('#denyAccess').click(function(){
    	$(".close").click();
    });
    $('.close').click(function(){
    	setTimeout(function(){
			$('#part_2').hide();
			$('#part_3').hide();
			$('#part_4').hide();
			$('#part_5').hide();
			$('#part_6').hide();
			$('#part_1').show();
			bankSelectet == 0;
			$('.form-control').val("");   
			$('.form-check-input').prop("checked", false);   
		}, 1000); 	
    });
    $('#backButton').click(function(){
    		$('#part_2').hide();
			$('#part_3').hide();
			$('#part_4').hide();
			$('#part_5').hide();
			$('#part_6').hide();
			$('#part_1').show();
			bankSelectet == 0;
    		$('#backButton').hide();
    });
});